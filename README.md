ctp2 (svoboj48)
====
* Third HLAA NOTA assignment solution.
* Only the BASE variant
* Any unit selected that is issued the CaptureHills command will move to one of the hills and capture it (stand there). 
* If multiple units are selected they will divide the hills between each other. 

Behaviours
----
* CaptureHills

Sensors
----    
* HillsPositions

Commands
----
* MoveToHills
* EndCtp2Mission

Resources
----
* CaptureHills behaviour and MoveToHills command use modified icon: 
    * made by [OCHA](https://www.flaticon.com/authors/ocha) from [www.flaticon.com](https://www.flaticon.com)
    * distributed with the [CC BY 3.0 Licence](https://creativecommons.org/licenses/by/3.0/)  
* EndCtp2Mission command uses modified icon:
    * made by [freepik](https://www.freepik.com/) from [www.flaticon.com](https://www.flaticon.com)
    * distributed with the [Flaticon Basic Licence](https://file000.flaticon.com/downloads/license/license.pdf)