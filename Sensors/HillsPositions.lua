local sensorInfo = {
	name = "hillsPositions",
	desc = "Return an array of positions of hills on the map. Each position is a table with x, y and z coordinates",
	author = "svobodajak",
	date = "2019-04-23",
	license = "notAlicense",
}

 -- we calculate it just once, does not change in a single run (hillHeight is const and tileSize needs to get reloaded)
local EVAL_PERIOD_DEFAULT = math.huge;

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(hillHeight, tileSize)
	-- calculating tiles count and setting up flag array
	local tilesXCount = Game.mapSizeX / tileSize;
	local tilesZCount = Game.mapSizeZ / tileSize;
	local hillInTile = {};

	-- mark tiles that are same height 
	for i = 1, tilesXCount, 1 
	do
		hillInTile[i] = {}
		for j = 1, tilesZCount, 1 
		do
			local y = Spring.GetGroundHeight(i * tileSize, j * tileSize)
			if(y >= hillHeight) 
			then
				hillInTile[i][j] = 1;
			else
				hillInTile[i][j] = 0;
			end
		end
	end

	
	-- add to the hill positions a single position per continous component (hill)
	local hillsPositions = {};
	local hillsPositionsIndex = 1;

	-- left up tile
	if(hillInTile[1][1] == 1) 
	then
		hillsPositions[hillsPositionsIndex] = Vec3(tileSize, hillHeight, tileSize);
		hillsPositionsIndex = hillsPositionsIndex + 1;
	end

	-- up tiles
	for j = 2, tilesZCount, 1 
	do
		if (hillInTile[1][j] == 1) 
		then
			if (hillInTile[1][j - 1] == 0) 
			then
				hillsPositions[hillsPositionsIndex] = Vec3(tileSize, hillHeight, j * tileSize);
				hillsPositionsIndex = hillsPositionsIndex + 1;
			end
		end
	end

	-- left tiles
	for i = 2, tilesXCount, 1 
	do
		if (hillInTile[i][1] == 1) 
		then
			if (hillInTile[i - 1][1] == 0) 
			then
				hillsPositions[hillsPositionsIndex] = Vec3(i * tileSize, hillHeight, tileSize);
				hillsPositionsIndex = hillsPositionsIndex + 1;
			end
		end
	end

	-- inner tiles
	for i = 2, tilesXCount, 1 
	do
		for j = 2, tilesZCount, 1 
		do
			if (hillInTile[i][j] == 1) 
			then
				if ((hillInTile[i][j - 1] == 0) and (hillInTile[i - 1][j] == 0) and (hillInTile[i - 1][j - 1] == 0)) 
				then
					hillsPositions[hillsPositionsIndex] = Vec3(i * tileSize, hillHeight, j * tileSize);
					hillsPositionsIndex = hillsPositionsIndex + 1;
				end
			end
		end
	end

	return hillsPositions;
end