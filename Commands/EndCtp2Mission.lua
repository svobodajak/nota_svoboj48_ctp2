-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

function getInfo()
	return {
    onNoUnits = SUCCESS,
    tooltip = "This node ends the ctp2 mission. Always returns SUCCESS"
	}
end



function Run(self, unitIds)
	
  message.SendRules({
		subject = "CTP_playerTriggeredGameEnd",
		data = {},
	})

  return SUCCESS;
end
