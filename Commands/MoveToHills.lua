function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Distributes units to hill positions. Returns RUNNING when units are moving to the positions. Returns SUCCEED when a unit reaches a hill position. Returns FAILURE otherwise",
		parameterDefs = {
			{ 
				name = "HillsPositions",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end


-- keep a global index of the hills to cycle through them
local currentHillIndex = 1;
local MOVE_COMMAND_ID = 10;

function Run(self, unitIds, parameters)
	local hillsPositions = parameters.HillsPositions;

	-- assign units to hills
	if(self.unitIdsToHill == nil) 
	then
		self.unitIdsToHill = {};
		self.lastUnitsFinishedCount = 0;
		for i = 1, #unitIds, 1 
		do
			self.unitIdsToHill[unitIds[i]] = hillsPositions[currentHillIndex];

			currentHillIndex = currentHillIndex + 1;
			if (currentHillIndex > #hillsPositions) 
			then 
				currentHillIndex = 1; 
			end
		end	
		return RUNNING;
	end

	-- issue move command
	for i = 1, #unitIds, 1
	do
		local unitHill = self.unitIdsToHill[unitIds[i]];
		Spring.GiveOrderToUnit(unitIds[i], MOVE_COMMAND_ID, {  
			unitHill.x, 
			unitHill.y, 
			unitHill.z}, {});
	end

	-- all units are at their position
	if(self.lastUnitsFinishedCount >= #unitIds)
	then
		return SUCCESS;
	end

	-- calculate how many units finished moving
	local finishedCount = 0;
	for i = 1, #unitIds, 1 
	do
		local isDead = Spring.GetUnitIsDead(unitIds[i]);
		-- if the unit is dead it is "finished" moving to the hill
		if (isDead == true or isDead == nil) 
		then
			finishedCount = finishedCount + 1;
		end

		-- the unit is at the hill position -> it is finished
		local unitX, unitY, unitZ = Spring.GetUnitPosition(unitIds[i]);
		local unitPositionVec = Vec3(unitX, unitY, unitZ);
		local hillPosition = self.unitIdsToHill[unitIds[i]];
		if(unitPositionVec:Distance(hillPosition) < 10) 
		then
			finishedCount = finishedCount + 1;
		end
	end

	-- additional unit has finished 
	if(finishedCount ~= self.lastUnitsFinishedCount) 
	then
		self.lastUnitsFinishedCount = finishedCount;
		return SUCCESS;
	end

	return RUNNING;
end
